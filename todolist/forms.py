from django import forms

class LoginForm(forms.Form):
	"""docstring for LoginForm"""
	username = forms.CharField(label='Username', max_length=20)
	password = forms.CharField(label='Password', max_length=20)

class AddTaskForm(forms.Form):
	"""docstring for AddTaskForm"""
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)

class UpdateTaskForm(forms.Form):
	"""docstring for UpdateTaskForm"""
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)
	status = forms.CharField(label='Status', max_length=50)

class Register(forms.Form):
	"""docstring for Register"""
	username = forms.CharField(label='Userame', max_length=50)
	first_name = forms.CharField(label='First Name', max_length=50)
	last_name = forms.CharField(label='Last Name', max_length=50)
	email = forms.CharField(label='Email', max_length=50)
	password = forms.CharField(label='Password', max_length=50)
	confirm_password = forms.CharField(label='Confirm Password', max_length=50)

# Capstone
class AddEventForm(forms.Form):
	"""docstring for AddEventForm"""
	event_name = forms.CharField(label='Event Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)
	event_date = forms.CharField(label='Event Date', max_length=50)


class UpdateEventForm(forms.Form):
	"""docstring for UpdateEventForm"""
	event_name = forms.CharField(label='Event Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)
	status = forms.CharField(label='Status', max_length=50)
	event_date = forms.CharField(label='Event Date', max_length=50)
		
		