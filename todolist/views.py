from django.shortcuts import render, redirect, get_object_or_404
# from django.http import HttpResponse
from .models import ToDoItem, ToDoEvent
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, Register, AddEventForm, UpdateEventForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from django.contrib.auth.hashers import make_password
# 'from' keyword allows importing of necessary classes, methods and other items needed in our application. 
#  While 'import' keyword defines what we are importing from the package

# Create your views here.

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	todoevent_list = ToDoEvent.objects.filter(user_id=request.user.id)
	# return HttpResponse("Hello from the views.py file")  # Session 1

	# output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
	# return HttpResponse(output)

	# template = loader.get_template("todolist/index.html")

	context = {
		'todoitem_list': todoitem_list,
		'todoevent_list': todoevent_list,
		'user': request.user
	}
	# return HttpResponse(template.render(context, request))
	return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
	# response = "You are viewing the details of %s"
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
	# return HttpResponse(response % todoitem_id)
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))


def todoevent(request, todoevent_id):
	todoevent = get_object_or_404(ToDoEvent, pk=todoevent_id)
	return render(request, "todolist/todoevent.html", model_to_dict(todoevent))

def register(request):
	# users = User.objects.all()
	# is_user_registered = False
	# context = {
	# 	"is_user_registered": is_user_registered
	# }

	# for indiv_user in users:
	# 	if indiv_user.username == "johndoe":
	# 		is_user_registered = True
	# 		break

	# if is_user_registered == False:
	# 	user = User()
	# 	user.username = "johndoe"
	# 	user.first_name = "John"
	# 	user.last_name = "Doe"
	# 	user.email = "john@mail.com"
	# 	user.set_password("john1234")
	# 	user.is_staff = False
	# 	user.is_active = True
	# 	user.save()
	# 	context = {
	# 		"first_name": user.first_name,
	# 		"last_name": user.last_name
	# 	}
	context = {}

	if request.method == 'POST':
		form = Register(request.POST)

		if form.is_valid() == False:
			form = Register()

		else:

			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			confirm_password = form.cleaned_data['confirm_password']


			if confirm_password==password:

				duplicates = User.objects.filter(email=email)

				if not duplicates:
					User.objects.create(username=username, first_name=first_name, last_name=last_name, email=email, password=make_password(password), date_joined=timezone.now())
					return redirect('todolist:login')

				else:
					context = {
						"error": True
					}
			else:
				context = {
					"error": True
				}

	return render(request, "todolist/register.html", context)

def change_password(request):

	is_user_authenticated = False

	user = authenticate(username="johndoe", password="john1234")
	print(user)
	if user is not None:
		authenticated_user = User.objects.get(username='johndoe')
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True
		context = {
			"is_user_authenticated": is_user_authenticated
		}

	return render(request, "todolist/change_password.html", context)

def login_view(request):
	# username = "johndoe"
	# password = "johndoe1"
	# user = authenticate(username=username, password=password)
	# context = {
	# 	"is_user_authenticated": False
	# }
	# print(user)
	# if user is not None:
	# 		login(request, user)
	# 		return redirect("todolist:index")
	# else:
	# 	return render(request, "todolist/login.html", context)

	context = {}

	if request.method == 'POST':
		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()

		else:
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			context = {
				"username": username,
				"password": password
			}
			if user is not None:
				login(request, user)
				return redirect("todolist:index")
			else:
				context = {
					"error": True
				}

	return render(request, "todolist/login.html", context)


def logout_view(request):
	logout(request)
	return redirect("todolist:index")

def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name=task_name)

			if not duplicates:
				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
				return redirect('todolist:index')

			else:
				context = {
					"error": True
				}
	return render(request, "todolist/add_task.html", context)

def update_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	context = {
		"user": request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status": todoitem[0].status
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()
				return redirect("todolist:index")
			else:
				context = {
					"error": True
				}
	return render(request, "todolist/update_task.html", context)

def delete_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
	return redirect("todolist:index")




def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			event_date = form.cleaned_data['event_date']

			duplicate_event = ToDoEvent.objects.filter(event_name=event_name)

			if not duplicate_event:
				ToDoEvent.objects.create(event_name=event_name, description=description, event_date=event_date, date_created=timezone.now(), user_id=request.user.id)
				return redirect('todolist:index')

			else:
				context = {
					"error": True
				}
	return render(request, "todolist/add_event.html", context)

def update_event(request, todoevent_id):
	todoevent = ToDoEvent.objects.filter(pk=todoevent_id)

	context = {
		"user": request.user,
		"todoevent_id": todoevent_id,
		"event_name": todoevent[0].event_name,
		"description": todoevent[0].description,
		"event_date": todoevent[0].event_date,
		"status": todoevent[0].status
	}

	if request.method == 'POST':
		form = UpdateEventForm(request.POST)

		if form.is_valid() == False:
			form = UpdateEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']
			event_date = form.cleaned_data['event_date']

			if todoevent:
				todoevent[0].event_name = event_name
				todoevent[0].description = description
				todoevent[0].status = status
				todoevent[0].event_date = event_date

				todoevent[0].save()
				return redirect("todolist:index")
			else:
				context = {
					"error": True
				}
	return render(request, "todolist/update_event.html", context)


def delete_event(request, todoevent_id):
	todoevent = ToDoEvent.objects.filter(pk=todoevent_id).delete()
	return redirect("todolist:index")




